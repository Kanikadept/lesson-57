import React from 'react';
import './EqualResult.css';

const EqualResult = props => {
    return (
        <div className="order-details">
            <p>Общая сумма: {props.totalPrice()} </p>
            <p>Количество человек: {props.pplCount}</p>
            <p>Каждый платит по: {props.equalPayResult()}</p>
        </div>
    );
};

export default EqualResult;