import React, {useState} from 'react';
import './DoCalc.css'

const DoCalc = props => {

    const [pplCount, setPplCount] = useState(0);
    const [orderPrice, setOrderPrice] = useState(0);
    const [tipPercentage, setTipPercentage] = useState(0);
    const [delivery, setDelivery] = useState(0);

    const handleSubmit = (e) => {
        e.preventDefault();
        props.changeInfo(pplCount, orderPrice, tipPercentage, delivery);
    }

    return (
        <div className="Do-calc">
            <form onSubmit={handleSubmit}>
                <div className="field">
                    <label>Человек: </label>
                    <div className="input">
                        <input value={pplCount} onChange={e => setPplCount(parseInt(e.target.value))} type="text"/>чел
                    </div>
                </div>
                <div className="field">
                    <label>Сумма заказа: </label>
                    <div className="input">
                        <input value={orderPrice} onChange={e => setOrderPrice(parseInt(e.target.value))} type="text"/>сом
                    </div>
                </div>
                <div className="field">
                    <label>Процент чаевых: </label>
                    <div className="input">
                        <input value={tipPercentage} onChange={e => setTipPercentage(parseInt(e.target.value))} type="text"/>%
                    </div>
                </div>
                <div className="field">
                    <label>Доставка: </label>
                    <div className="input">
                        <input value={delivery} onChange={e => setDelivery(parseInt(e.target.value))} type="text"/>сом
                    </div>
                </div>

                <button className="do-cal-btn">Расчитать</button>
            </form>
        </div>
    );
};

export default DoCalc;