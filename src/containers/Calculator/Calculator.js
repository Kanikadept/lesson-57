import React, {useState} from 'react';
import './Calculator.css'
import DoCalc from "./DoCalc/DoCalc";
import EqualResult from "./DoCalc/EqualResult/EqualResult";
import DoCalcSep from "../../components/DoCalcSep/DoCalcSep";


const Calculator = () => {
    const [mode, setMode] = useState('first');

    const [orderInfo, setOrderInfo] = useState(
        {
            pplCount: 0,
            orderPrice: 0,
            tipPercentage: 0,
            delivery: 0,
        });

    const [orderInfo2, setOrderInfo2] = useState([
        {name: 'Petr', price: 100},
        {name: 'Zhenya', price: 200},
        {name: 'Michael', price: 300},
    ]);

    const handleRadioChange = (e) => {
        console.log(e.target.value);
        setMode(e.target.value);
    }

    const changeInfo = (pplCount, orderPrice, tipPercentage, delivery) => {

        const orderInfoCopy = {...orderInfo};
        orderInfoCopy.pplCount = pplCount;
        orderInfoCopy.orderPrice = orderPrice;
        orderInfoCopy.tipPercentage = tipPercentage;
        orderInfoCopy.delivery = delivery;
        setOrderInfo(orderInfoCopy);
    }

    const addOrderInfo2 = (name, price) => {
        setOrderInfo2([...orderInfo2, {name: price, price: price}]);
    }

    const getTotalPrice = () => {
        return ((orderInfo.tipPercentage / 100) * orderInfo.orderPrice) + orderInfo.orderPrice + orderInfo.delivery;
    }

    const equalPayResult = () => {
        return Math.ceil(getTotalPrice() / orderInfo.pplCount);
    }

    return (
        <div className="Calculator">
            <p>Сумма заказа считается:</p>
            <div className="Equally"><input type="radio" value="first" name="option" checked={mode === 'first'} onChange={handleRadioChange}/> Поровну между всеми участниками</div>
            <div className="Equally"><input type="radio" value="second" name="option" checked={mode === 'second'} onChange={handleRadioChange}/> Каждому индивидуально</div>
            {mode === 'first' && (
                <>
                    <DoCalc orderInfo={orderInfo} changeInfo={changeInfo}/>
                    <EqualResult totalPrice={getTotalPrice} pplCount={orderInfo.pplCount} equalPayResult={equalPayResult}/>
                </>
            )}

            {mode === 'second' && (
                <DoCalcSep orderInfo2={orderInfo2} addOrderInfo2={addOrderInfo2}/>
            )}

        </div>
    );
};

export default Calculator;