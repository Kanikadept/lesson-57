import React, {useState} from 'react';
import './DoCalcSep.css';
import EqualResultSep from "./EqualResultSep/EqualResultSep";

const DoCalcSep = props => {

    const [inputFields, setInputFields] = useState([
        {name: '', price: 0}
    ]);

    const [ppl, setPpl] = useState([])

    const [tipPercentage, setTipPercentage] = useState(0);
    const [delivery, setDelivery] = useState(0);


    const handleSubmit = (e) => {
        e.preventDefault();

        inputFields.forEach( field => {
            setPpl((ppl) => ([...ppl, {name: field.name, price: field.price}]));
        })
    }

    const handleAddFields = () => {
        setInputFields([...inputFields, {name: '', price: 0}]);
    }

    const handleRemoveField = (index) => {
        const inputFieldsCopy = [...inputFields];
        inputFieldsCopy.splice(index, 1);
        setInputFields(inputFieldsCopy);
    }

    const handleChangeInput = (index, event) => {
        const inputFieldsCopy = [...inputFields];
        if (event.target.name === "name") {
            inputFieldsCopy[index][event.target.name] = event.target.value;
        } else {
            inputFieldsCopy[index][event.target.name] = parseInt(event.target.value);
        }

        setInputFields(inputFieldsCopy);
    }

    const getTotalPrice = () => {
        return ppl.reduce((acc, unit) => {
                acc += (((tipPercentage / 100) * unit.price) + unit.price);

                return acc;
            }, 0)

    }

    return (
        <div className="do-calc-sep">
            <form onSubmit={(e) => handleSubmit(e)}>

                {inputFields.map((inputField, index) => (
                    <div className="input-row" key={index}>
                        <input name="name" type="text" placeholder="name" onChange={event => handleChangeInput(index, event)}/>
                        <label><input name="price" type="text" placeholder="price" onChange={event => handleChangeInput(index, event)}/> сом</label>
                        <button onClick={() => handleRemoveField(index)}>delete</button>
                    </div>
                ))}

                <input onClick={() => handleAddFields()} type="button" value="+"/>
                <div>Процент чаевых: <input type="text" value={tipPercentage} onChange={(e) => setTipPercentage(e.target.value)}/> %</div>
                <div>Доставка: <input type="text" value={delivery} onChange={(e) => setDelivery(e.target.value)}/> сом</div>
                <button>Расчитать</button>
            </form>
            <EqualResultSep getTotalPrice={getTotalPrice} ppl={ppl} tipPercentage={tipPercentage} delivery={delivery}/>
        </div>
    );
};

export default DoCalcSep;