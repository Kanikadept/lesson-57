import React from 'react';

const EqualResultSep = props => {

    let totalPrice = null;
    if (props.getTotalPrice() > 0) {
        totalPrice = <p>Общая сумма: {props.getTotalPrice()}</p>;
    }
    return (
        <div>
            {totalPrice}
            {props.ppl.map(man => {
                return <p>{man.name} : {Math.ceil((props.tipPercentage / 100) * man.price + man.price) + (props.delivery / props.ppl.length)} сом</p>

            })
            }
        </div>
    );
};

export default EqualResultSep;